package locators;

public class CommunalPaymentsLocators extends BaseLocators {
    public String paymentsCatalogHeader = "//div[@data-qa-file='PageTitleContainer']//div[@data-qa-file='PaymentsCatalogHeader']";

    public String regionChangeLink = "//span[@role='button' and contains(@class, 'regionSelect')]";

    public String providerLink = "//li[@data-qa-file='UIMenuItemProvider']//a[@href='/payments/provider-%s/']/span[text()]";

    public String firstProviderLink = "//li[@data-qa-file='UIMenuItemProvider'][1]//a[@href='/payments/provider-%s/']/span[text()]";
}