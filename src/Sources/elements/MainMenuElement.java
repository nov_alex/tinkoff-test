package elements;

import locators.MainMenuLocators;
import org.openqa.selenium.WebDriver;
import pages.BasePage;
import validators.MainMenuValidators;

public class MainMenuElement extends BasePage {

    public MainMenuValidators validators;
    public MainMenuLocators locators;

    public MainMenuElement(WebDriver driver) {
        super(driver);
        this.locators = new MainMenuLocators();
        this.validators = new MainMenuValidators(this);
    }

    public String getMenuItemName(String href) {
        return waiters.waitForElement(locators.getElement(locators.menuItemLink, href))
                .getText().replace("&nbsp;"," ");
    }

    public void clickMenuItemBtn(String href) {
        driver.findElement(locators.getElement(locators.menuItemLink, href)).click();
    }
}