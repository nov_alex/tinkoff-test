package elements;

import locators.RegionLocators;
import org.openqa.selenium.WebDriver;
import pages.BasePage;

public class RegionElement extends BasePage {

    private RegionLocators locators;

    public RegionElement(WebDriver driver) {
        super(driver);
        this.locators = new RegionLocators();
    }

    public void setCity(String city) {
        waiters.waitForElement(locators.getElement(locators.regionLink, city)).click();
    }
}