package pages;

import locators.CommunalPaymentsLocators;
import org.openqa.selenium.WebDriver;

public class CommunalPaymentsPage extends BasePage {

    private CommunalPaymentsLocators locators;

    public CommunalPaymentsPage(WebDriver driver) {
        super(driver);
        this.locators = new CommunalPaymentsLocators();
    }

    public String getPaymentsCatalogHeaderName() {
        return waiters.waitForElement(locators.getElement(locators.paymentsCatalogHeader))
                      .getText().replace("&nbsp;"," ");
    }

    public void clickRegionChangeBtn() {
        waiters.waitForElement(locators.getElement(locators.regionChangeLink)).click();
    }

    public String getProviderName(String provider) {
        return waiters.waitForElement(locators.getElement(locators.providerLink, provider))
                      .getText().replace("&nbsp;"," ");
    }

    public void clickProviderBtn(String provider) {
        waiters.waitForElement(locators.getElement(locators.providerLink, provider)).click();
    }

    public String getFirstProviderName(String provider) {
        return waiters.waitForElement(locators.getElement(locators.firstProviderLink, provider))
                .getText().replace("&nbsp;"," ");
    }

    public void clickFirstProviderBtn(String provider) {
        waiters.waitForElement(locators.getElement(locators.firstProviderLink, provider)).click();
    }
}