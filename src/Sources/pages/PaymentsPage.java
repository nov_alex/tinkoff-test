package pages;

import locators.PaymentsLocators;
import org.openqa.selenium.WebDriver;
import validators.PaymentsValidators;

public class PaymentsPage extends BasePage {

    public PaymentsValidators validators;
    public PaymentsLocators locators;

    public PaymentsPage(WebDriver driver) {
        super(driver);
        this.locators = new PaymentsLocators();
        this.validators = new PaymentsValidators(this);
    }

    public String getPaymentsMenuItemName(String href) {
        return waiters.waitForElement(locators.getElement(locators.paymentsMenuItemLink, href))
                .getText().replace("&nbsp;"," ");
    }

    public void clickPaymentsMenuItemBtn(String href) {
        driver.findElement(locators.getElement(locators.paymentsMenuItemLink, href)).click();
    }
}