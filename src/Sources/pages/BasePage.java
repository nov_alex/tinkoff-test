package pages;

import helpers.Waiters;
import org.openqa.selenium.WebDriver;

public class BasePage {

    public WebDriver driver;
    public Waiters waiters;

    protected BasePage(WebDriver driver) {
        this.driver = driver;
        this.waiters = new Waiters(driver);
    }
}
